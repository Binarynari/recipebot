# -*- coding: utf-8 -*-
import re
import urllib.request
import json
from urllib.parse import quote

from bs4 import BeautifulSoup

from flask import Flask, request
from slack import WebClient
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slackeventsapi import SlackEventAdapter

from slack.web.classes.interactions import MessageInteractiveEvent

SLACK_TOKEN = ""
SLACK_SIGNING_SECRET = ""


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 레시피 출력
def _crawl(text):

    #사용자의 입력을 받아 제일 첫번째 링크를 출력
    a = text.split(' ')
    foodname = quote(a[1])
    global block_id_2
    block_id_2 = a[1]

    url = "http://www.10000recipe.com/recipe/list.html?q=" + foodname
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    section = soup.find("div", class_="col-xs-4")
    
    #제일 첫번째 링크를 출력
    recipe_address = section.find("a", class_="thumbnail")['href']
    recipe_url = "http://www.10000recipe.com" + recipe_address

    
    #제목, 링크, 글쓴이 가져오기
    items = []
    for item_div in soup.find_all("div", class_="col-xs-4"):
        title = item_div.find("div", class_="caption")
        maker = None
        if title:
            title = title.get_text().strip()
            title_list = title.split("\n")
            title = title_list[0]
            maker = title_list[1]

        link = item_div.find("a", class_="thumbnail")
        if link:
           link = "http://www.10000recipe.com" + link["href"]

        items.append({
            "title":title,
            "link":link,
            "maker":maker,
            "summary":""
        })
    ###############################################################추가
    #조리정보 크롤링
    for item in items[:4]:
        link_recipe_url = item["link"] #변수명 변경
        link_source_code = urllib.request.urlopen(link_recipe_url).read()
        link_soup = BeautifulSoup(link_source_code, "html.parser")

        summary_info="\n"
        for summary in link_soup.find_all("div", class_="view2_summary_info"):
            info1=summary.find("span", class_="view2_summary_info1")
            if info1:
                summary_info += ":bust_in_silhouette:" + info1.get_text() + "\t"
                #print("인포1 O")
                
            info2=summary.find("span", class_="view2_summary_info2")
            if info2:
                summary_info += ":stopwatch:"+info2.get_text() + "\t"
                #print("인포2 O")
                
            info3=summary.find("span", class_="view2_summary_info3")
            if info3:
                summary_info += ":star:"+info3.get_text()
                #print("인포3 O")
        item["summary"]=summary_info
    ###############################################################
   #그림을 넣는 코드    

    block_list = []

    img_list = []
    # img_section = soup.find_all("div", class_="rcp_m_list2")
    # print(img_section)
    img_items=[]
    for img_section in soup.find_all("div", class_="rcp_m_list2"):
        for img in img_section.find_all("a", class_="thumbnail"):
            if len(img_list) <= 4:
                for imges in img.find_all("img"):           
                    img_items.append(imges.get("src"))
                img_list.append(img_items[-1])

    #1. head section 구성
    head_section = SectionBlock(
        text="\n\n*" + a[1] + " 레시피를 찾으세요?*",
    )
    block_list.append(head_section)
    block_list.append(DividerBlock())
    
    #2. 리스트 섹션 구성

    for i in range(4):
        item = items[i]
        item["title"].replace("<", "[").replace(">","]")
        item_string = "*<" + item["link"] + "|" + "[" + item["title"] + "]>*\n" +item["summary"] + "\n\n"+ str(item["maker"]) + '\n\n'

        item_image = ImageElement(
            image_url=img_list[i],
            alt_text="음식사진"
        )
        link_section = SectionBlock(
            text=item_string,
            accessory=item_image
        )
        block_list.append(link_section)

        button_section = ActionsBlock(
            # block_id = a[1],
            elements=[
                    ButtonElement(
                    text="레시피 바로보기",
                    action_id="recipe_" + str(i + 1), value=str(i + 1)
                ),
            ]
        )
        block_list.append(button_section)
        block_list.append(DividerBlock())

    # print(block_list)
    # return [head_section, link_section, button_actions]
    return block_list

def month_crawl(text, start):
    url = "http://www.10000recipe.com/ranking/list.html?type=best"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    title_list = soup.find_all("h4", class_="media-heading el_title")
    
    #타이틀의 레시피
    titles = []
    urls = []
    items = []
    for title in title_list:
        recipe_address = title.find("a")['href']
        recipe_url = "http://www.10000recipe.com" + recipe_address
        title = title.get_text().strip()

        titles.append(title)
        urls.append(recipe_url)
        items.append({
            "title" : title,
            "link" : recipe_url,
            "summary":""
        })

    #조리정보 크롤링
    for item in items[start:start+3]:
        link_recipe_url = item["link"] #변수명 변경
        link_source_code = urllib.request.urlopen(link_recipe_url).read()
        link_soup = BeautifulSoup(link_source_code, "html.parser")

        summary_info="\n"
        for summary in link_soup.find_all("div", class_="view2_summary_info"):
            info1=summary.find("span", class_="view2_summary_info1")
            if info1:
                summary_info += ":bust_in_silhouette:" + info1.get_text() + "\t"
                #print("인포1 O")
                
            info2=summary.find("span", class_="view2_summary_info2")
            if info2:
                summary_info += ":stopwatch:"+info2.get_text() + "\t"
                #print("인포2 O")
                
            info3=summary.find("span", class_="view2_summary_info3")
            if info3:
                summary_info += ":star:"+info3.get_text()
                #print("인포3 O")
        item["summary"]=summary_info

    img_list = []
    for img in soup.find_all("div", class_="media-left"):
        tag = img.find("img")
        if(tag != None):
            img_list.append(tag.get("src"))
    



    #더보기 버튼
    block_list = []
    if start == 0: #제일 처음에 불려졌을때
        month_title_section = SectionBlock(
            text="*[이달의 추천 레시피]*"
        )
        block_list.append(month_title_section)
 
    end = start + 3
    if(start ==9):
        end = 10
    for i in range(start, end):
        item = items[i]
        item["title"].replace("<", "[").replace(">","]")
        item_string = ":heart: " + " *<" + item["link"] + "|" + item["title"] + ">*\n\n"+item["summary"] + "\n\n"
        
        item_image = ImageElement(
            image_url=img_list[i],
            alt_text="음식사진"
        )
        
        link_section = SectionBlock(
            text=item_string,
            accessory=item_image
        )
        block_list.append(link_section)
    
        button_section = ActionsBlock(
            block_id = item["link"],
            elements=[
                    ButtonElement(
                    text="레시피 바로보기",
                    action_id="month",
                    value = str(9999)
                ),
            ]
        )
        block_list.append(button_section)
        block_list.append(DividerBlock())

        
    if start < 9: #더보기 버튼 필요        
        button_section = ActionsBlock(
            elements=[
                    ButtonElement(
                    text="더보기",
                    action_id="more_" + str(start + 3), value=str(start + 3)
                ),
            ]
        )
        block_list.append(button_section)

    return block_list

#@Recipebot 음식이름 / 추천 / 인기검색어 / 노하우` 라고 멘션해보세용"
def usage_crawl(text):
    
    #추천 / 인기검색어 / 노하우 버튼
    block_list = []
 
    month_title_section = SectionBlock(
        text="*멘션이 귀찮으시면?*"
    )
    block_list.append(month_title_section)
    '''
    button_section = ActionsBlock(
        block_id = text,
        elements=[
                ButtonElement(
                text="아무거나!",
                action_id="anything_", value = 0
            ),
            
            ButtonElement(
                text="이달의 추천",
                action_id="recommand_", value = 0
            ),
            ButtonElement(
                text="인기검색어",
                action_id="popular_", value = 0
            ),
            ButtonElement(
                text="요리 노하우",
                action_id="knowhow_", value = 0
            ),
            
        ]
    )
    '''
    button_section = ActionsBlock(
        elements=[
                ButtonElement(
                text="아무거나!",
                action_id="anything_", value=str(0)
            ),
                ButtonElement(
                text="이달의 추천",
                action_id="recommand_", value=str(0)
            ),
                ButtonElement(
                text="인기검색어!",
                action_id="popular_", value=str(0)
            ),
                ButtonElement(
                text="요리 노하우",
                action_id="knowhow_", value=str(0)
            ),
        ]
    )
    block_list.append(button_section)

    return block_list


def knowhow_crawl(text):
    url = "http://www.10000recipe.com/issue/view.html?cid=10000know"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    title_list = soup.find_all("a", class_="thumbnail")
    
    #타이틀의 레시피
    titles = []
    urls = []
    items = []
    i = 0
    for title in title_list:
        recipe_address = title['href']
        #print("\n recipe address : " + recipe_address)
        recipe_url = "http://www.10000recipe.com" + recipe_address
        title = title.find("span", class_="jq_elips2").get_text().strip()
        #print("\n title : " + title)
        #print(" recipe_url : " + recipe_url)

        titles.append(title)
        
        urls.append(recipe_url)
        items.append({
            "title" : title,
            "link" : recipe_url,
        })
        i += 1
        if i > 11: break
    
    #그림을 넣는 코드    
    
    knowhow_image = ImageElement(
        image_url="https://media.gettyimages.com/photos/grinning-cook-holding-ladle-picture-iddv271137a",
        alt_text="꿀팁 대방출!!"
    )
    
    knowhow_title_section = SectionBlock(
        text="*:pizza::hamburger:다양한 요리 노하우:+1::+1:*",
        accessory = knowhow_image
    )
    

    
    item_fields = ""
    imogi_list = ["rice_ball", "cooking", "stew", "bowl_with_spoon", "popcorn", "lollipop", "custard", "cake", "shaved_ice", "sushi"]
    j = 0
    for item in items[:10]:
        item["title"].replace("<", "[").replace(">","]")
        text = ":" + imogi_list[j] + ":" + " *<" + item["link"] + "|" + item["title"] + ">*\n\n"
        item_fields += text
        j += 1
    
    title_section = SectionBlock(text = item_fields)
 
    return [knowhow_title_section, DividerBlock(), title_section, DividerBlock()]


# 조리과정 출력
def step_crawl(text, num):
    if num == 9999:
        recipe_url = text
    else:
        foodname = quote(text)
        url = "http://www.10000recipe.com/recipe/list.html?q=" + foodname
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")
        section = section = soup.find("div", class_="col-xs-4")
        recipe_address = section.find("a", class_="thumbnail")['href']
            
        if(num!=1):        #num번째 주소
            section = soup.find_all("div", class_="col-xs-4")
            recipe_address = section[num-1].find("a", class_="thumbnail")['href']

        
        #레시피 상세페이지에서 재료 크롤링
        recipe_url = "http://www.10000recipe.com" + recipe_address #변수명 변경
    source_code = urllib.request.urlopen(recipe_url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    ingredients = [] #재료 리스트
    for ingredient in soup.find_all("div", class_ = "cont_ingre2"):
        for data in ingredient.find_all("li"):
            if len(ingredients) < 10:
                temp_data = ':chopsticks: ' + data.get_text().strip().replace(' ', '').replace('\n', '')
                ingredients.append(temp_data)
    if len(ingredients) == 0:
        ingredients.append("재료 정보가 없습니다.")

    #레시피 상세페이지에서 조리순서 크롤링
    steps = "*[조리과정]*\n\n"
    for step in soup.find_all("div", class_="view_step"):
        index = 0
        for data in step.find_all("div", class_="view_step_cont"):
            steps = steps + ':cooking: ' + str(index + 1) + '. ' + data.get_text().strip() + '\n\n'
            index = index + 1
    steps = steps + "\n\n원본 링크 : " + recipe_url


    ingredients_title_section = SectionBlock(text="*[재료 리스트]*")
    
    #레시피 상세페이지에서 대표이미지 크롤링
    img_items=[]
    for img in soup.find_all("div", class_="centeredcrop"):
        for imges in img.find_all("img"):           
            img_items.append(imges.get("src"))
   
    #조리과정 대표사진 Image Block
    steps_image_section = ImageBlock(
        title=text,
        image_url=img_items[-1],
        alt_text="이미지가 안 보일 때 대신 표시할 텍스트"
    )

        #조리정보 크롤링
    summary_info="\n"
    for summary in soup.find_all("div", class_="view2_summary_info"):
        info1=summary.find("span", class_="view2_summary_info1")
        if info1:
            summary_info += ":bust_in_silhouette:"+info1.get_text() + "\t"
            #print("인포1 O")
        
        
        info2=summary.find("span", class_="view2_summary_info2")
        if info2:
            summary_info += ":stopwatch:"+info2.get_text() + "\t"
            #print("인포2 O")
        
        
        info3=summary.find("span", class_="view2_summary_info3")
        if info3:
            summary_info += ":star:"+info3.get_text() + "\t"
            #print("인포3 O")
    
    if len(summary_info) == 1:      ##################################추가 
        summary_info = "조리 정보가 없습니다."

    summary_title_section = SectionBlock(text="*[조리 정보]*")  ##################################추가   
   

    #조리정보 Section Block
    summary_section = SectionBlock(text=summary_info)
    
    #재료 Section Block
    ingredients_section = SectionBlock(fields=ingredients)

    #조리순서 Section Block
    steps_section = SectionBlock(text=steps)    

    
    #제목 Section Block
    temp = soup.find("div", class_="view2_summary")
    title = '*[ ' + temp.find("h3").get_text().strip() + ' ]*'
    title_section = SectionBlock(text=title)


    return [DividerBlock(), title_section, steps_image_section,summary_title_section, summary_section, DividerBlock(),ingredients_title_section, ingredients_section, DividerBlock(), steps_section, DividerBlock()]

def word_rank():
    rank_list=[]

    url = "http://www.10000recipe.com/ranking/list.html?type=keyword"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    words=[]
    word_link=[]
    num = ['0','1','2','3','4','5','6','7','8','9']
    for word_div in soup.find_all("div",class_="search_word"):
        for word_li in word_div.find_all("li"):
            word = word_li.get_text()
            for i in range(0,len(word)):
                if word[i] in num:
                    word = word[:i]
                    break
            #menu = word   
            words.append(word)
            
        for word_a in word_div.find_all("a"):
            word_href = "http://www.10000recipe.com"+word_a['href']
            word_link.append(word_href)

    word_title_section = SectionBlock(text="*[:mag:이달의 인기검색어]*")
    rank_list.append(word_title_section)

    number = ['one','two','three','four','five','six','seven','eight','nine','keycap_ten']
    #keywords=""
    for x in range(10):
        text = ":" + number[x] + ":" + " *<" + word_link[x] + "|" + words[x] + ">*"+"\n"
        #keywords += text
        link_section = SectionBlock(
            text=text,
            accessory=ButtonElement(
                text=words[x] + " 레시피 찾기",
                action_id="rank_" + str(i + 1), value=str(words[x]))
        )
        rank_list.append(link_section)


    #word_section = SectionBlock(text=keywords)
    #return [word_title_section, word_section]
    return rank_list


prev_client_msg_id = {}
# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    
    #메시지의 중복 수신을 방지하는 코드
    client_msg_id = event_data["event"]["client_msg_id"]
    if client_msg_id not in prev_client_msg_id:
        prev_client_msg_id[client_msg_id] = 1
    else:
        return

    message_blocks = ""

    if "추천" in text:
        message_blocks = month_crawl(text, 0)
    elif "인기검색어" in text :
        message_blocks = word_rank()
    elif "노하우" in text :
        message_blocks = knowhow_crawl(text)
    elif "사용법" in text or "usage" in text:
        slack_web_client.chat_postMessage(
            channel=channel,
            text="`@Recipebot 음식이름 / 추천 / 인기검색어 / 노하우` 라고 멘션해보세용"
        )
        message_blocks = usage_crawl(text)
    else:
        message_blocks = _crawl(text)

    #extract_json()또는
    #json.dump() 안에 []를 넣어도 ok
  
    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(message_blocks),
    )

# 사용자가 버튼을 클릭한 결과는 /click 으로 받습니다
# 이 기능을 사용하려면 앱 설정 페이지의 "Interactive Components"에서
# /click 이 포함된 링크를 입력해야 합니다.
@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리합니다
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))
    message_blocks = []
    if "recipe" in click_event.action_id:
        #keyword = click_event.block_id
        keyword = block_id_2
        recipe_num = int(click_event.value)
        message_blocks = step_crawl(keyword, recipe_num)
    elif "more" in click_event.action_id or "recommand" in click_event.action_id:
        keyword = "추천"
        index_num = int(click_event.value)
        message_blocks = month_crawl(keyword, index_num)
    elif "anything" in click_event.action_id:
        random_food=["찜닭", "수박주스", "파스타", "짜장면", "국밥", "삼계탕", "피자", "쿠키", "아몬드", "리조또"]
        index_num = (random.randrange(0,9))
        message_blocks = _crawl("<0> "+random_food[index_num])
    elif "popular" in click_event.action_id:
        message_blocks = word_rank()
    elif "knowhow" in click_event.action_id:
        message_blocks = knowhow_crawl("노하우")
    elif "month" in click_event.action_id:
        keyword = click_event.block_id
        recipe_num = int(click_event.value)
        message_blocks = step_crawl(keyword, recipe_num)
    elif "rank" in click_event.action_id:
        index_num = click_event.value
        message_blocks = _crawl("레시피 "+index_num)

    # 메시지를 채널에 올립니다
    slack_web_client.chat_postMessage(
        channel=click_event.channel.id,
        blocks=extract_json(message_blocks)
    )

    # Slack에게 클릭 이벤트를 확인했다고 알려줍니다
    return "OK", 200


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
